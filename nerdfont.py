import argparse
import subprocess
from collections import defaultdict
from pathlib import Path


def load_db(dbfile: Path):
    """
    :return: dict; key=nf-group-name, value="\x00"
    """

    def source():
        with dbfile.open("rb") as fp:
            for line in fp:
                name, codepoint = line.decode().split(" ", maxsplit=1)
                assert name.startswith("nf-")
                yield name[3:], codepoint

    return dict(source())


class Facts:
    dbfile = Path(__file__).resolve().parent.joinpath("var", "nerdfont.db")
    db = load_db(dbfile)


def choose_some_to_view():
    input = "\n".join(Facts.db.keys()).encode()

    try:
        cp = subprocess.run(
            ["fzf", "-m"], input=input, stdout=subprocess.PIPE, check=True
        )
    except subprocess.CalledProcessError as e:
        raise SystemExit(e.returncode) from e

    names = cp.stdout.decode().splitlines()

    for name in names:
        try:
            codepoint = Facts.db[name]
        except KeyError:
            pass
        else:
            print(chr(int(codepoint, 16)), end=" ")
    print("")


def view_all():
    grouped = defaultdict(list)

    for _name, codepoint in Facts.db.items():
        try:
            group, name = _name.split("-", maxsplit=1)
        except ValueError as e:
            raise SystemExit(f"invalid name: {_name}") from e
        grouped[group].append((name, codepoint))

    for group, items in grouped.items():
        print(group)
        for _, codepoint in items:
            print(chr(int(codepoint, 16)), end=" ")
        print("")


def parse_args(args=None):

    parser = argparse.ArgumentParser()
    parser.add_argument("subcmd", choices=("fzf", "all"))

    return parser.parse_args(args)


if __name__ == "__main__":
    args = parse_args()

    if args.subcmd == "fzf":
        choose_some_to_view()
    elif args.subcmd == "all":
        view_all()
    else:
        parse_args(["-h"])
