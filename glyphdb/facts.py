import pathlib


class Filesystem:
    root = pathlib.Path(__file__).resolve().parent.parent
    var = root.joinpath("var")


class Proxy:
    http = "http://127.0.0.1:8118"
