import hashlib
import json
import logging

import httpx
import lxml.html
import trio

from glyphdb import facts


async def create_nerdfont_db():
    """db formats: "\\w+ [0-9a-f]+" """

    url = "https://www.nerdfonts.com/cheat-sheet"
    cachekey = hashlib.md5(url.encode()).hexdigest()
    cachefile = facts.Filesystem.var.joinpath(f"{cachekey}.html")

    try:
        fp = await trio.open_file(cachefile, "rb")
    except FileNotFoundError:
        logging.debug("loading source from %s", url)

        client = httpx.AsyncClient(proxies=httpx.Proxy(facts.Proxy.http))
        async with client:
            resp = await client.request("GET", url)
            data = resp.content
        async with await trio.open_file(cachefile, "wb") as fp:
            await fp.write(data)
    else:
        logging.debug("loading source from local cache")
        async with fp:
            data = await fp.read()

    dbfile = facts.Filesystem.var.joinpath("nerdfont.db")
    html = lxml.html.fromstring(data)

    def source():
        for container in html.xpath(
            '//div[@id="glyphCheatSheet"]/div[@class="column"]'
        ):
            name = container.findtext('div[@class="class-name"]')
            codepoint = container.findtext('div[@class="codepoint"]')

            yield f"{name} {codepoint}\n".encode()

    async with await trio.open_file(dbfile, "wb") as fp:
        logging.debug("saving nerdfont db in %s", dbfile)
        for line in source():
            await fp.write(line)


async def create_emoji_db():
    """db formats: "\\w+ [0-9a-f]+" """

    url = "https://api.github.com/emojis"
    cachekey = hashlib.md5(url.encode()).hexdigest()
    cachefile = facts.Filesystem.var.joinpath(f"{cachekey}.json")

    try:
        fp = await trio.open_file(cachefile, "rb")
    except FileNotFoundError:
        logging.debug("loading source from %s", url)

        client = httpx.AsyncClient(proxies=httpx.Proxy(facts.Proxy.http))
        async with client:
            resp = await client.request("GET", url)
            data = resp.content
        async with await trio.open_file(cachefile, "wb") as fp:
            await fp.write(data)
    else:
        logging.debug("loading source from local cache")
        async with fp:
            data = await fp.read()

    dbfile = facts.Filesystem.var.joinpath("emoji.db")
    # dict[name, url]
    jata: dict = json.loads(data)

    def source():
        prefix_len = len("https://github.githubassets.com/images/icons/emoji/unicode/")
        suffix_len = len(".png?v8")
        # codepoint = \w+(-\w+){,3}
        for name, url in jata.items():
            codepoint = url[prefix_len + 1 : -suffix_len]
            if not codepoint:
                logging.error("%s has no correspond codepoint, skipped", name)
                continue
            if "-" in codepoint:
                logging.error("%s is a codepoint sequence, skipped", name)
                continue
            yield f"{name} {codepoint}\n".encode()

    async with await trio.open_file(dbfile, "wb") as fp:
        logging.debug("saving emoji db in %s", dbfile)
        for line in source():
            await fp.write(line)


async def main():
    await create_nerdfont_db()
    await create_emoji_db()


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{asctime} {message}",
    )

    trio.run(main)
